//
//  TestModel.swift
//  CodableGenerics
//
//  Created by Gene Crucean on 11/23/19.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

// MARK: - Post
struct Post: Codable {
    let userID, id: Int
    let title, body: String

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case id, title, body
    }
}

typealias Posts = [Post]
