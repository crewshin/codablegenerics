import Foundation

// MARK: - Endpoints
extension URLSession {
    func nonGeneric(with url: URL, requestType: HTTPRequestType = .get, additionalHeaders: [String: String]? = nil, completion: @escaping (Posts?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, requestType: requestType, additionalHeaders: additionalHeaders, completion: completion)
    }
    
    func generic<T: Codable>(with url: URL, requestType: HTTPRequestType = .get, additionalHeaders: [String: String]? = nil, body: T.Type? = nil, completion: @escaping (Post?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.genericCodableTask(with: url, requestType: requestType, additionalHeaders: additionalHeaders, body: body, completion: completion)
    }
}
