//
//  ViewController.swift
//  CodableGenerics
//
//  Created by Gene Crucean on 11/23/19.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func nonGenericTapped(_ sender: Any) {
        let url = URL(string: "https://jsonplaceholder.typicode.com/posts")!
        let task = URLSession.shared.nonGeneric(with: url, requestType: .get) { (posts, response, error) in
            print("posts: \(posts)")
        }
        task.resume()
    }
    
    @IBAction func genericTapped(_ sender: Any) {
        let url = URL(string: "https://jsonplaceholder.typicode.com/posts")!
        let testBody = TestBody(title: "foo", body: "bar", userId: 1)
        let task = URLSession.shared.genericCodableTask(with: url, requestType: .post, body: testBody) { (post, response, error) in
            print("post: \(post)")
        }
        task.resume()
    }
}

