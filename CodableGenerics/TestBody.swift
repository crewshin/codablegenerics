//
//  TestBody.swift
//  CodableGenerics
//
//  Created by Gene Crucean on 11/23/19.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

struct TestBody: Codable {
    let title: String
    let body: String
    let userId: Int
}
