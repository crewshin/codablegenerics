import Foundation

// MARK: - Request Types
enum HTTPRequestType: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
    case head = "HEAD"
    case connect = "CONNECT"
    case options = "OPTIONS"
    case trace = "TRACE"
    case patch = "PATCH"
}

// MARK: - Helper functions for creating encoders and decoders
func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .formatted(DateFormatter.iso8601Full)
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .formatted(DateFormatter.iso8601Full)
    }
    return encoder
}

extension DateFormatter {
    static let iso8601Full: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
}

// MARK: - URLSession response handlers
extension URLSession {
    func globalHeaders(additionalHeaders: [String: String]?) -> [String: String] {
        
        var headers = [
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8",
        ]
        
        if let additionalHeaders = additionalHeaders {
            additionalHeaders.forEach {
                headers[$0.0] = $0.1
            }
        }
        
        return headers
    }
    
    func codableTask<T: Codable>(with url: URL, requestType: HTTPRequestType = .get, additionalHeaders: [String: String]? = nil, completion: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        var request = URLRequest(url: url, cachePolicy: .reloadRevalidatingCacheData, timeoutInterval: 30.0)
        request.httpMethod = requestType.rawValue
        request.allHTTPHeaderFields = globalHeaders(additionalHeaders: additionalHeaders)
        
        return dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else {
                completion(nil, response, error)
                return
            }
            completion(try? newJSONDecoder().decode(T.self, from: data), response, nil)
        }
    }
    
    func genericCodableTask<T: Codable>(with url: URL, requestType: HTTPRequestType = .get, additionalHeaders: [String: String]? = nil, body: T.Type? = nil, completion: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        var request = URLRequest(url: url, cachePolicy: .reloadRevalidatingCacheData, timeoutInterval: 30.0)
        request.httpMethod = requestType.rawValue
        request.allHTTPHeaderFields = globalHeaders(additionalHeaders: additionalHeaders)

        // This is where things went sideways :) Basically I just wanted to support optional bodies for whatever codable type I passed in.
        request.httpBody = try? newJSONEncoder().encode(body as? Encodable)

        return dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else {
                completion(nil, response, error)
                return
            }
            completion(try? newJSONDecoder().decode(T.self, from: data), response, nil)
        }
    }
}
